package app.pool.server.utils;

import app.pool.utils.FileContentUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class FileUtilsTest {

    @Test
    public void test() {

        List<FileContentUtils.FilePart> filePartList = new ArrayList<>();
        {
            FileContentUtils.FilePart filePart = new FileContentUtils.FilePart();
            filePart.setStart(1);
            filePart.setLength(2);
            filePart.setData(new byte[]{1, 2});
            filePartList.add(filePart);
        }
        {
            FileContentUtils.FilePart filePart = new FileContentUtils.FilePart();
            filePart.setStart(4);
            filePart.setLength(2);
            filePart.setData(new byte[]{4, 5});
            filePartList.add(filePart);
        }

        FileContentUtils.FileParts parts = new FileContentUtils.FileParts(filePartList);
        byte[] original = {0, 1, 2, 3, 4, 5, 6, 7};
        byte[] newcontent = FileContentUtils.deleteFileContent(original, parts);
        Assert.assertArrayEquals(new byte[]{0, 3, 6, 7}, newcontent);

        byte[] afterRestore = FileContentUtils.restoreDeletedFileContent(newcontent, parts);
        Assert.assertArrayEquals(original, afterRestore);

    }

}
