package app.pool.server.utils;

import app.pool.utils.FileContentUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class FilePartsTest {

    @Test
    public void test() {
        {
            List<FileContentUtils.FilePart> filePartList = new ArrayList<>();
            {
                FileContentUtils.FilePart filePart = new FileContentUtils.FilePart();
                filePart.setStart(0);
                filePart.setLength(2);
                filePartList.add(filePart);
            }

            FileContentUtils.FileParts fileParts = new FileContentUtils.FileParts(filePartList);
            FileContentUtils.FileParts newFileParts = fileParts.calcLeaveParts(10);
            List<FileContentUtils.FilePart> newFilePartList = newFileParts.getFileParts();
            Assert.assertEquals(1, newFilePartList.size());
            Assert.assertEquals(2, newFilePartList.get(0).getStart());
            Assert.assertEquals(8, newFileParts.allFilePartLength());
        }

        {
            List<FileContentUtils.FilePart> filePartList = new ArrayList<>();
            {
                FileContentUtils.FilePart filePart = new FileContentUtils.FilePart();
                filePart.setStart(8);
                filePart.setLength(2);
                filePartList.add(filePart);
            }

            FileContentUtils.FileParts fileParts = new FileContentUtils.FileParts(filePartList);
            FileContentUtils.FileParts newFileParts = fileParts.calcLeaveParts(10);
            List<FileContentUtils.FilePart> newFilePartList = newFileParts.getFileParts();
            Assert.assertEquals(1, newFilePartList.size());
            Assert.assertEquals(0, newFilePartList.get(0).getStart());
            Assert.assertEquals(8, newFileParts.allFilePartLength());
        }

        {
            List<FileContentUtils.FilePart> filePartList = new ArrayList<>();
            {
                FileContentUtils.FilePart filePart = new FileContentUtils.FilePart();
                // 0,1
                filePart.setStart(0);
                filePart.setLength(2);
                filePartList.add(filePart);
            }

            {
                FileContentUtils.FilePart filePart = new FileContentUtils.FilePart();
                // 2,3
                filePart.setStart(2);
                filePart.setLength(2);
                filePartList.add(filePart);
            }

            {
                FileContentUtils.FilePart filePart = new FileContentUtils.FilePart();
                // 5,6
                filePart.setStart(5);
                filePart.setLength(2);
                filePartList.add(filePart);
            }

            FileContentUtils.FileParts fileParts = new FileContentUtils.FileParts(filePartList);
            FileContentUtils.FileParts newFileParts = fileParts.calcLeaveParts(10);
            List<FileContentUtils.FilePart> newFilePartList = newFileParts.getFileParts();
            Assert.assertEquals(2, newFilePartList.size());
            Assert.assertEquals(4, newFilePartList.get(0).getStart());
            Assert.assertEquals(1, newFilePartList.get(0).getLength());
            Assert.assertEquals(7, newFilePartList.get(1).getStart());
            Assert.assertEquals(3, newFilePartList.get(1).getLength());
            Assert.assertEquals(4, newFileParts.allFilePartLength());

        }
    }

}
