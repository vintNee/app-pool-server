package app.pool.storage;

import lombok.Getter;
import lombok.Setter;

import java.io.File;

@Getter
@Setter
public class CacheEntryInfo {

    private String md5;
    private int length;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (!(o instanceof CacheEntryInfo)) {
            return false;
        }

        CacheEntryInfo entryInfo = (CacheEntryInfo) o;

        if (length != entryInfo.length) return false;
        return md5 != null ? md5.equals(entryInfo.md5) : entryInfo.md5 == null;
    }

    @Override
    public int hashCode() {
        int result = md5 != null ? md5.hashCode() : 0;
        result = 31 * result + length;
        return result;
    }

    @Override
    public String toString() {
        return "CacheEntryInfo{" +
                "md5='" + md5 + '\'' +
                ", length=" + length +
                '}';
    }

    public String cacheFilename() {
        return this.length + "_" + this.md5;
    }

    public String tmpCacheFilename() {
        return this.length + "_" + this.md5 + ".tmp";
    }

    public static boolean isCachefile(String name) {
        return name.contains("_")
                && !name.contains(".");
    }
}
