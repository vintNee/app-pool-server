package app.pool.storage;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
public class FileData {

    private MultipartFile file;
    private String entryInfos;
    private String md5;

}
