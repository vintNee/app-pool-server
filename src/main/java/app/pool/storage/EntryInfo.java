package app.pool.storage;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class EntryInfo extends CacheEntryInfo {

    private String name;
    private int start;

    public static EntryInfo findByName(List<EntryInfo> existedEntryInfos, String name) {
        for (EntryInfo existedEntryInfo : existedEntryInfos) {
            if(existedEntryInfo.name.equals(name)){
                return existedEntryInfo;
            }
        }
        return null;
    }
}
