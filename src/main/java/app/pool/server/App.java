package app.pool.server;

import app.pool.storage.StorageMethods;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;

import java.io.File;

public class App {

    private static Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) throws Exception {
        if (args.length < 1) {
            System.out.println("请输入应用包存放目录");
            System.exit(1);
        }

        String dirPath = args[0];
        File dir = new File(dirPath);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        StorageMethods.setBaseDir(dir);

        SpringApplication.run(new Object[]{AppConfig.class}, args);
        logger.info("Base Dir=" + dir.getCanonicalPath());
    }

}


