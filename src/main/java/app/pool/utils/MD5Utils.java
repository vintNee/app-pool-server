package app.pool.utils;


import java.security.NoSuchAlgorithmException;

public class MD5Utils {

    /**
     * 生成MD5
     */
    public static String getMD5(String source) {
        return getMD5(source.getBytes());
    }

    /**
     * 生成MD5
     */
    public static String getMD5(byte[] source) {

        String s = null;

        // 用来将字节转换成 16 进制表示的字符
        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'a', 'b', 'c', 'd', 'e', 'f'};
        try {

            java.security.MessageDigest md = java.security.MessageDigest
                    .getInstance("MD5");
            md.update(source);

			/*
             *  MD5 的计算结果是一个 128 位的长整数，用字节表示就是 16 个字节，
			 *  每个字节用 16 进制表示的话，使用两个字符，所以表示成 16 进制需要 32 个字符。
			 *  不采用String.format进行格式化，String.format比较慢，慢差不多50倍。
			 */
            byte tmp[] = md.digest();

            char str[] = new char[16 * 2];

            int k = 0;
            for (int i = 0; i < 16; i++) {
                // 从第一个字节开始，对 MD5 的每一个字节
                // 转换成 16 进制字符的转换
                byte byte0 = tmp[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            s = new String(str);

        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }

        return s;
    }

}
