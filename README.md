# app-pool-server

#### 项目介绍
上传文件到远程机房速度一般比较慢，例如100KB/s。使用putty工具上传war包（40M）到远程服务器需要几分钟。

为了提高效率，减少上传文件时间，我们可以尽量减少要传输的文件内容。

war包是zip压缩文件，同一个应用，不同版本的war包，会包含很多相同的压缩文件。上传文件时，如果不需要上传服务器端已经存在的压缩文件，那就可以大大减少要传输的文件内容。


#### 实现原理

1. 客户端在上传文件前，解析文件，计算压缩文件中每个Entry内容的MD5值。

2. 客户端将Entry内容的MD5值和长度发送到服务器端，检查服务器是否已存在对应的Entry。

3. 服务器根据Entry内容的MD5值和文件长度进行判断，检查服务器端存在哪些Entry，并返回给客户端。

4. 客户端将服务器端已存在的Entry从文件中删除，生成新文件（压缩文件内容）。

5. 客户端将新文件，和已去掉的Entry列表发送给服务器端。

6. 服务器端将去掉的Entry还原到上传的文件中（还原文件内容）。

7. 服务器端将文件解压缩，将文件包含的Entry建立缓存。

![multisources](img/design.jpg)

#### 运行效果
方案 | 需要传输的文件大小 | 上传耗时
--- | -- | --
优化前 | 14330 KB | 140 秒
优化后 |    90 KB |   5 秒


#### 安装教程
项目编译 mvn clean install

#### 使用说明
需要与[app-pool-client客户端](https://gitee.com/jscode/app-pool-client)配合一起使用

1. 启动http服务器
```
java -jar .\target\app-pool-server.jar d:\tmp  #第一个参数为应用包存放目录
```

2. 使用客户端app-pool-client上传文件
```
java -jar target/app-pool-client.jar http://localhost d:\tmp\jar-demo.jar # 第一个参数为服务器地址，第二个参数为上传的文件。参考https://gitee.com/jscode/app-pool-client
```

3. 查看已上传的文件
```
用浏览器打开http://localhost
```

![multisources](img/result.jpg)

#### 文件缓存
- 服务器端只缓存大小超过100KB的文件

